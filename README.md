# Overview

This is my first example of using Embassy with esp32 devices using Rust.

Generated project using: `cargo generate esp-rs/esp-template`

## Env Setup & Build

### One-Time
```shell
cargo install cargo-generate
cargo install ldproxy
cargo install espup
cargo install espflash
cargo install cargo-espflash # Optional  
 ```

### Every Time

```shell
source $HOME/export-esp.sh
```

